# Exemple Portfolio

> Comment présenter simplement ses projets de formation Simplon Dev Data ?

Le code de la page web est dans le dossier `src`: [src/index.html](src/index.html)

La page web est déployée automatiquement avec [GitLab Pages](https://docs.gitlab.com/ce/user/project/pages/)
et est accessible [en ligne](https://gitlab.com/Laurent38/portfolio_perso/).

